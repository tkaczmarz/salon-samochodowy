<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Edycja samochodu</title>
</head>
<body>

<jsp:useBean id="storage" class="com.tkaczmarz.salon.service.StorageService" scope="application"></jsp:useBean>
<jsp:useBean id="car" class="com.tkaczmarz.salon.domain.Car" scope="session" />
<jsp:setProperty property="*" name="car"/>

<%
  storage.edit (car);
%>

<h2> Samochod zostal zmieniony </h2>
<p><a href="showAllCars.jsp"> Wroc do listy samochodow </a></p>

</body>
</html>