<%@page import="com.tkaczmarz.salon.domain.Car"%>
<%@page import="com.tkaczmarz.salon.service.StorageService"%>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.List" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Wszystkie samochody</title>
</head>
<body>
<jsp:useBean id="storage" class="com.tkaczmarz.salon.service.StorageService" scope="application" />

<%
  List<Car> cars = new ArrayList<Car> ();
  cars = storage.getAllCars();

  if (cars.size() > 0)
  {
	  out.println ("Samochodow w bazie: " + cars.size() + " <br/>");
	  out.println ("<table border=1><thead><tr>");
	  out.println ("<th>Marka</th> <th>Model</th> <th>Rok produkcji</th>");
	  out.println ("<th>Moc</th> <th>Pojemnosc</th> <th>Paliwo</th>");
	  out.println ("<th colspan=2>Akcja</th>");
	  out.println ("</tr></thead><tbody>");
	  
	  for (int i = 0; i < cars.size(); i++) {
		  out.println ("<tr>");
		  out.println ("<td>" + cars.get(i).getMarka() + "</td>");
		  out.println ("<td>" + cars.get(i).getModel () + "</td>");
		  out.println ("<td>" + cars.get(i).getRokProdukcji () + "</td>");
		  out.println ("<td>" + cars.get(i).getMoc () + "</td>");
		  out.println ("<td>" + cars.get(i).getPojemnosc () + "</td>");
		  out.println ("<td>" + cars.get(i).getPaliwo () + "</td>");
		  out.println ("<td><a href=edit?id=" + i + "> Edytuj </a></td>");
		  out.println ("<td><a href=delete?id=" + i + "> Usun </a></td>");
		  out.println ("</tr>");
	  }
	  out.println ("</tbody></table>");
  }
  else
  {
	  out.println ("<html><body><h1> Brak samochodow w bazie </h1></body></html>");
  }
%>

<p>
  <a href="getCarData.jsp">Dodaj samochod</a>
</p>
<p>
  <a href="index.jsp">Wroc do menu glownego</a>
</p>
</body>
</html>