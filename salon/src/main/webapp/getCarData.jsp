<%@page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Dodaj samochod</title>
</head>
<body>

<jsp:useBean id="storage" class="com.tkaczmarz.salon.service.StorageService" scope="application" />
<jsp:useBean id="car" class="com.tkaczmarz.salon.domain.Car" scope="session" />

<form action="addCar.jsp">

  Marka<br/>
  <input type="text" pattern=".{1,30}" required name="marka" value="brak" /><br />
  Model<br/>
  <input type="text" pattern=".{1,30}" required name="model" value="brak" /><br />
  Rok produkcji (1900 - <%= Calendar.getInstance().get(Calendar.YEAR) %>)<br/>
  <input type="number" required min="1900" max="<%= Calendar.getInstance().get(Calendar.YEAR) %>" 
  		name="rokProdukcji" value="<%= Calendar.getInstance().get(Calendar.YEAR) %>" /><br />
  Pojemnosc silnika<br/>
  <input type="number" min="0" required name="pojemnosc" value="0" /><br />
  Moc<br/>
  <input type="number" min="0" required name="moc" value="0" /><br />
  Paliwo<br/>
  <input type="text" pattern=".{1,30}" required name="paliwo" value="brak" /><br />
  <input type="submit" value="Dodaj"/>
</form>
</body>
</html>