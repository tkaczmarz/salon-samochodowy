<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Samochod dodany</title>
</head>
<body>

<jsp:useBean id="car" class="com.tkaczmarz.salon.domain.Car" scope="session"/>
<jsp:setProperty name="car" property="*"/>
<jsp:useBean id="storage" class="com.tkaczmarz.salon.service.StorageService" scope="application"/>

<% 
  storage.add (car); 
%>

<p>Samochod <jsp:getProperty name="car" property="marka"/> 
			<jsp:getProperty name="car" property="model"/> zostal dodany do bazy.</p>
<p> <a href="getCarData.jsp">Dodaj kolejny samochod</a> </p>
<p> <a href="showAllCars.jsp">Pokaz wszystkie samochody</a> </p>

</body>
</html>