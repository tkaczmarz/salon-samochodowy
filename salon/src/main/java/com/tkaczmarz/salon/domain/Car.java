package com.tkaczmarz.salon.domain;

public class Car {

	private int id;
	private String marka;
	private String model;
	private int rokProdukcji;
	private int moc;
	private String paliwo;
	private int pojemnosc;
	
	public Car ()
	{
		
	}
	
	public Car (int id, String marka, String model, int rokProdukcji, int moc, String paliwo, int pojemnosc)
	{
		this.id = id;
		this.marka = marka;
		this.model = model;
		this.rokProdukcji = rokProdukcji;
		this.moc = moc;
		this.paliwo = paliwo;
		this.pojemnosc = pojemnosc;
	}
	
	public int getId ()
	{
		return id;
	}
	
	public void setId (int id)
	{
		this.id = id;
	}
	
	public String getMarka ()
	{
		return marka;
	}
	
	public void setMarka (String marka)
	{
		this.marka = marka;
	}
	
	public String getModel ()
	{
		return model;
	}
	
	public void setModel (String model)
	{
		this.model = model;
	}
	
	public int getRokProdukcji ()
	{
		return rokProdukcji;
	}
	
	public void setRokProdukcji (int rokProdukcji)
	{
		this.rokProdukcji = rokProdukcji;
	}
	
	public int getMoc ()
	{
		return moc;
	}
	
	public void setMoc (int moc)
	{
		this.moc = moc;
	}
	
	public String getPaliwo ()
	{
		return paliwo;
	}
	
	public void setPaliwo (String paliwo)
	{
		this.paliwo = paliwo;
	}
	
	public int getPojemnosc ()
	{
		return pojemnosc;
	}
	
	public void setPojemnosc (int pojemnosc)
	{
		this.pojemnosc = pojemnosc;
	}
}
