package com.tkaczmarz.salon.service;

import java.util.ArrayList;
import java.util.List;

import com.tkaczmarz.salon.domain.Car;

public class StorageService {
	
	private List<Car> cars = new ArrayList<Car> ();
	
	public void add (Car car)
	{		
		cars.add(new Car (cars.size (),car.getMarka(), car.getModel(), car.getRokProdukcji(), 
							car.getMoc(), car.getPaliwo(), car.getPojemnosc()));
		
		RefreshIDs ();
	}
	
	public void delete (int id)
	{
		cars.remove(id);
		
		RefreshIDs ();
	}
	
	public void edit (Car car)
	{
		//cars.set(car.getId(), car);
		cars.get(car.getId()).setMarka(car.getMarka());
		cars.get(car.getId()).setModel(car.getModel());
		cars.get(car.getId()).setRokProdukcji(car.getRokProdukcji());
		cars.get(car.getId()).setMoc(car.getMoc());
		cars.get(car.getId()).setPaliwo(car.getPaliwo());
		cars.get(car.getId()).setPojemnosc(car.getPojemnosc());
		
		RefreshIDs ();
	}
	
	public List<Car> getAllCars ()
	{
		return cars;
	}
	
	private void RefreshIDs ()
	{
		for (int i = 0; i < cars.size(); i++)
		{
			cars.get(i).setId (i);
		}
	}
}
