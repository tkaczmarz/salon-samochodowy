package com.tkaczmarz.salon.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tkaczmarz.salon.domain.Car;
import com.tkaczmarz.salon.service.*;

@WebServlet(urlPatterns = "/delete")
public class DeleteCarServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private StorageService storageService;
       
    public DeleteCarServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		RequestDispatcher view = request.getRequestDispatcher("showAllCars.jsp");
		
		storageService = (StorageService) getServletContext ().getAttribute("storage");
		
		if (request.getParameter("id") == null)
		{
	        view.forward(request, response);
		}
		else
		{
			int carID = Integer.parseInt(request.getParameter("id"));
			List<Car> cars = storageService.getAllCars();
			
			if (cars.size() <= Integer.parseInt(request.getParameter("id")))
			{
				view.forward(request, response);
			}
			else
			{
				if (cars.get(carID) != null)
				{
					out.println("<html><body><h2>" + cars.get(carID).getMarka() + " " + cars.get(carID).getModel() +
							" zostal usuniety.</h2><br/><p><a href=showAllCars.jsp>Powrot</a></p></body></html>");
				
					storageService.delete(carID);					
				}				
			}
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
