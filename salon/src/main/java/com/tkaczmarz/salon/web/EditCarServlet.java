package com.tkaczmarz.salon.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tkaczmarz.salon.domain.Car;
import com.tkaczmarz.salon.service.*;

@WebServlet(urlPatterns = "/edit")
public class EditCarServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public EditCarServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 
		int carID = Integer.parseInt(request.getParameter("id"));
		StorageService storage = (StorageService) getServletContext ().getAttribute("storage");
		List<Car> cars = (List<Car>) storage.getAllCars();
		
		PrintWriter out = response.getWriter();
		
		out.println("<html><body><form action=editCar.jsp>");
		out.println("<input type=hidden name=id value=" + cars.get(carID).getId() + " />");
		out.println("Marka<br/> <input type=text pattern=.{1,30} required name=marka value=" + cars.get(carID).getMarka() + " /> <br/>");
		out.println("Model<br/> <input type=text pattern=.{1,30} required name=model value=" + cars.get(carID).getModel() + " /> <br/>");
		out.println("Rok produkcji (1900 - " + Calendar.getInstance().get(Calendar.YEAR) + ")<br/> <input type=number min=1900 max=" + 
					Calendar.getInstance().get(Calendar.YEAR) + " required name=rokProdukcji value=" + cars.get(carID).getRokProdukcji() + " /> <br/>");
		out.println("Pojemnosc silnika<br/> <input type=number required min=0 name=pojemnosc value=" + cars.get(carID).getPojemnosc() + " /> <br/>");
		out.println("Moc<br/> <input type=number required min=0 name=moc value=" + cars.get(carID).getMoc() + " /> <br/>");
		out.println("Paliwo<br/> <input type=text pattern=.{1,30} required name=paliwo value=" + cars.get(carID).getPaliwo() + " /> <br/>");
		out.println("<input type=submit value=Zmien /> <br/>");
		out.println("</form></body></html>");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doGet(request, response);
	}

}